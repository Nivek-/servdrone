<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Serv'Drone | La technologie au service du quotidien</title>

    <!-- Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>

    <!-- Favicon -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/img/favicon.ico">
    <link rel="shortcut icon" href="assets/img/favicon.ico">

    <!-- Fonts import -->
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap"
        rel="stylesheet">

    <!-- Styles imports -->
    <link href="assets/css/styles.min.css" type="text/css" rel="stylesheet">

    <!-- Global metas -->
    <meta name="description"
        content="Serv’drone est une start-up française créée en 2016 par trois amis d’enfance passionnés par l’IoT (Les objets connectés). Leur objectif principal est d’allier technologie et service à la personne.">
    <meta name="author" content="Serv'Drone">
    <meta name="keywords" content="drone, technologie et service à la personne, service à la personne">

    <!-- Metas Facebook & Twitter for sharing -->
    <meta property="og:url" content="https://www.servdrone.fr" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Serv'Drone" />
    <meta property="og:description"
        content="Serv’drone est une start-up française créée en 2016 par trois amis d’enfance passionnés par l’IoT (Les objets connectés). Leur objectif principal est d’allier technologie et service à la personne." />
    <meta property="og:image" content="assets/img/logotype-servdrone.svg" />
    <meta name="twitter:card"
        content="Serv’drone est une start-up française créée en 2016 par trois amis d’enfance passionnés par l’IoT (Les objets connectés). Leur objectif principal est d’allier technologie et service à la personne." />
    <meta name="twitter:site" content="https://www.servdrone.fr" />
    <meta name="twitter:creator" content="Serv'Drone" />
</head>

<body>

    <!-- Button menu for mobile -->
    <button id="menu-hamburger" type="button"><img src="assets/img/menu.svg"
            alt="Icone ouvrant le menu version mobile"></button>

    <!-- Header -->
    <header>
        <!-- First part of menu (identity) -->
        <!-- Button for menu mobile -->
        <div class="menu-mobile-close">
            <h5>MENU</h5>
            <button type="button" id="button-close-menu">
                <i class="material-icons">close</i>
            </button>
        </div>

        <div class="identity-menu">
            <div class="identity-logo">
                <a href="index.html">
                    <img src="assets/img/logotype-servdrone.svg" alt="Logotype Serv'Drone">
                </a>
            </div>

            <div class="icons-menu">
                <!-- Search -->
                <div id="search">
                    <form id="search-bar" method="post" action="search.php">
                        <input class="input-text" type="text" name="search" maxlength="100" value=""
                            placeholder="Rechercher" />
                        <button type="submit" id="search-button-submit">
                            <i class="material-icons icon-menu">search</i>
                        </button>
                    </form>
                </div>

                <div class="align-icons-menu">
                    <!-- Shopping cart -->
                    <div class="margin-buttons-menu">
                        <button type="button" class="buttons-menu"><i
                                class="material-icons icon-menu">shopping_cart</i></button>
                    </div>

                    <!-- User connection / registration -->
                    <div class="margin-buttons-menu">
                        <button type="button" class="buttons-menu"><i class="far fa-user-circle icon-menu"></i></button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Nav -->
        <nav>
            <div class="navigation-menu">

                <!-- Lists items for navigation -->
                <ul class="navbar-style">

                <li class="nav-item"><a href="index.html" class="navbar-link">Accueil</a></li>
                    <li class="line-menu-separator"></li>
                    <li class="nav-item">
                        <a href="#" class="navbar-link">Les drones</a>
                        <ul class="submenu">
                            <li><a href="drone-class-1.html">Serv'Drone Class 1</a></li>
                            <li class="line-menu"></li>
                            <li><a href="drone-plus.html">Serv'Drone Plus</a></li>
                            <li class="line-menu"></li>
                            <li><a href="drone-deluxe.html">Serv'Drone Deluxe</a></li>
                            <li class="line-menu"></li>
                            <li><a href="drone-historique.html">Historique</a></li>
                        </ul>
                    </li>
                    <li class="line-menu-separator"></li>
                    <li class="nav-item">
                        <a href="evenements.html" class="navbar-link">Événements</a>
                    </li>
                    <li class="line-menu-separator"></li>
                    <li class="nav-item menu-societe">
                        <a href="#" class="navbar-link">La société</a>
                        <ul class="submenu">
                            <li><a href="fondateurs.html">Les fondateurs</a></li>
                            <li class="line-menu"></li>
                            <li><a href="medias.html">Les médias en parlent</a></li>
                        </ul>
                    </li>
                    <li class="line-menu-separator"></li>
                    <li class="nav-item menu-assistance">
                        <a href="#" class="navbar-link">Assistance</a>
                        <ul class="submenu">
                            <li><a href="contact.php">Contact</a></li>
                            <li class="line-menu"></li>
                            <li><a href="faq.html">FAQ</a></li>
                        </ul>
                    </li>

                </ul>
                <!-- End list items for navigation -->

            </div>
        </nav>
        <!-- End nav -->

    </header>
    <!-- End header -->


    <!-- Banniere -->
    <section id="banniere">
        <!-- Image de fond -->
        <div class="img-banniere img-banniere-contact">

            <!-- Titres banniere -->
            <div class="titles-banniere">
                <div class="title-one">
                    <h1>Contacter Serv'Drone</h1>
                </div>
                <div class="title-two">
                    <h2>
                        Vous avez une question, une demande ?<br>
                        N'hésitez pas à nous écrire
                    </h2>
                </div>
            </div>

        </div>
    </section>
    <!-- End banniere -->

    <section id="contact">
        <div class="container">
            <div class="contact-flex">

                <!-- Contact form -->
                <div class="form-contact">
                    <div class="form-alignement">
                        <h3>Envoyer un message</h3>
                        <div class="underline-form-contact"></div>

                        <!-- Form before send message -->
                        <div id="form-before">
                            <p class="contact-infos-small">*Champs obligatoires</p>

                            <form id="contact-form" method="POST" action="form-contact.php">

                                <select name="type-article" class="contact-form-style" required>
                                    <option value="">Sujet de votre demande*</option>
                                    <option value="renseignement_produit">Un renseignement sur un produit</option>
                                    <option value="renseignement_evenement">Un renseignement sur un événement</option>
                                    <option value="renseignement_autre">Autre</option>
                                </select>

                                <input type="email" name="email" class="contact-form-style" maxlength="200" placeholder="Votre adresse email*" required>

                                <input type="text" name="nom" class="contact-form-style" maxlength="150" placeholder="Votre nom*" required>

                                <input type="text" name="prenom" class="contact-form-style" maxlength="150" placeholder="Votre prénom*" required>

                                <textarea name="message" rows="3" class="contact-form-style" id="text-message-contact-form" maxlength="1500" placeholder="Votre message*" required></textarea>

                                <div class="boutons-check">
                                    <input type="checkbox" id="checked1" name="infos-accept-partage" value="oui" required />
                                    <p class="contact-checkbox">En appuyant sur le bouton "Envoyer", j'accepte de partager les informations saisies avec l'entreprise Serv'Drone et d'être recontacté(e) concernant ma demande.</p>
                                </div>

                                <div class="boutons-check">
                                    <input type="checkbox" id="checked2" name="infos-accept-politique" value="oui" required />
                                    <p class="contact-checkbox">J'ai lu et j'accepte la <a href="politique-confidentialite.html">politique de confidentialité</a>.</p>
                                </div>

                                <button type="submit" class="button buttons-style">Envoyer</button>
                            </form>
                        </div>
                        <!-- End form before send message -->

                        <!-- Form hide, show when message is send -->
                        <div id="form-after" class="form-display-none2">
                            <div class="alignement-form-after">
                                <i class='far fa-check-circle'></i>
                                <p>
                                    Votre demande a bien été envoyée.<br>
                                    <br>
                                    Vous recevrez une réponse de notre équipe dans les plus brefs délais.
                                </p>
                            </div>
                            
                            <button type="submit" class="button buttons-style" id="button-new-form-contact">Envoyer une autre demande</button>
                        </div>
                        <!-- End form hide, show when message is send -->
                    </div>
                </div>


                <!-- Informations contact -->
                <div class="infos-contact">
                    <h3>Où nous trouver ?</h3>
                    <div class="info-contact">
                        <i class='fas fa-map-marker-alt'></i> 
                        <p>
                            100 rue de Lorem Ipsum<br>
                            Lille, 59000
                        </p>
                    </div>
                    <div class="info-contact">
                        <i class='fas fa-phone-alt'></i> 
                        <p>
                            03 00 00 00 00<br>
                            <span class="text-secondary">Service disponible du lundi au vendredi, de 10h à 18h30 et le samedi de 9h à 13h.</span>
                        </p>
                    </div>
                    <div class="info-contact">
                        <i class='fas fa-envelope'></i> 
                        <p>
                            contact@servdrone.com
                        </p>
                    </div>

                    <div class="contact-map-google">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d40492.028081074175!2d3.061268054416442!3d50.631728256550986!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sfr!2sfr!4v1577282678306!5m2!1sfr!2sfr" frameborder="0" allowfullscreen="" class="map-contact"></iframe>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer>

        <!-- Newsletter -->
        <div id="newsletter">
                <h4>S'abonner à la newsletter</h4>
            <form>
                <input type="email" placeholder="Votre adresse email" name="newsletter" id="nl_input">
                <button type="submit" id="newsletter-button-submit">
                    <i class="fas fa-arrow-right icon-newsletter-inscription"></i>
                </button>
            </form>
        </div>
        <!-- End newsletter -->

        <ul id="top_footer">

            <!-- Logo and Social medias -->
            <li>
                <ul id="logo_social">
                    <li><a href="#"><img src="assets/img/logotype-servdrone-blanc.svg"
                                alt="logo serv'drone blanc" /></a></li>
                    <li>
                        <ul id="media">
                            <li><a href=""><i class="fa fa-facebook media"></i></a></li>
                            <li><a href=""><i class="fa fa-twitter media"></i></a></li>
                            <li><a href=""><i class="fa fa-instagram media"></i></a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <!-- End logo and Social medias -->

            <!-- Links into footer -->
            <li>
                <ul id="links">
                    <li><a href="confidentialite.html">Politique de confidentialité</a></li>
                    <li><a href="mentions-legales.html">Mentions légales</a></li>
                    <li><a href="cgv.html">Conditions générales de vente</a></li>
                    <li><a href="cgu.html">Conditions générales d'utilisation</a></li>
                    <li><a href="plandusite.html">Plan du site</a></li>
                </ul>
            </li>
            <!-- End links into footer -->

        </ul>

        <!-- Copyright -->
        <div id="copyright">
            <p>© 2020 Tous droits réservés Serv'Drone</p>
        </div>
        <!-- End Copyright -->

    </footer>
    <!--End footer-->
    <script src="assets/js/script.min.js"></script>
</body>

</html>