"use strict";

jQuery(document).ready(function ($) {
  //capture de l'élément formulaire
  var form = $('#contact-form'); //définition d'une action lors de la soumission du formulaire

  $(form).submit(function (event) {
    console.log("Ca marche"); //on empeche le formulaire de recharger la page

    event.preventDefault(); //on met toutes les données sous format d'une chaine de caractère

    var formData = $(this).serialize(); //on utilise AJAX pour la soumission du formulaire

    $.ajax({
      url: 'form-contact.php',
      type: 'POST',
      data: formData
    }).done(function (response) {
      console.log(response);
      $("#form-before").addClass("form-display-none");
      $("#form-after").removeClass("form-display-none2");
      $("#form-after").addClass("form-display-block");
      $("#contact-form :input").val("");
      $("#text-message-contact-form").val("");
      $("#checked1").attr('checked', false);
      $("#checked2").attr('checked', false);
    }).fail(function (response) {
      console.log(response);
    });
  });
});
document.addEventListener('DOMContentLoaded', function () {
  // Une fois que la page est prête (a chargé)
  // Afficher de nouveau le formulaire
  var formShow = document.getElementById("button-new-form-contact");
  var formBefore = document.getElementById('form-before');
  var formAfter = document.getElementById('form-after');
  formShow.addEventListener("click", function () {
    if (formBefore.className == "form-display-none") {
      formBefore.classList.remove("form-display-none");
      formAfter.classList.remove("form-display-block");
      formAfter.classList.add("form-display-none2");
    }
  });
});