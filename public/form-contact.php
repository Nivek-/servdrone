<?php
    $type_demande=null;
    $email=null;
    $nom=null;
    $prenom=null;
    $message=null;
    $partage=null;
    $politique=null;


    if(!empty($_POST['type-article']) && !empty($_POST['email']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['message']) && !empty($_POST['infos-accept-partage']) && !empty($_POST['infos-accept-politique'])){
        $type_demande=htmlspecialchars($_POST['type-article']);
        $email=htmlspecialchars($_POST['email']);
        $nom=htmlspecialchars($_POST['nom']);
        $prenom=htmlspecialchars($_POST['prenom']);
        $message=htmlspecialchars($_POST['message']);
        $partage=htmlspecialchars($_POST['infos-accept-partage']);
        $politique=htmlspecialchars($_POST['infos-accept-politique']);


        /* ----- Envoi d'un email reprennant les informations du client et sa demande à l'entreprise ----- */
        $to="fy.rouyer@gmail.com"; // Mail de l'entreprise
        $subject="[Serv'Drone] Message de $prenom $nom"; // Sujet du mail
        $message_client = "Message provenant du site Serv'Drone.
        Type de demande : $type_demande
        Nom de l'utilisateur : $nom
        Prénom de l'utilisateur : $prenom
        Message de l'utilisateur :
        $message"; // Message du mail

        // Informations : le mail va être envoyé à qui, on peut ajouter des cc/cci, quannd on clique sur répondre, répondre à qui + version de php
        $headers = 'From: servdrone@contact.com' . "\r\n" .
        'Reply-To: ' . $email . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        // Si le mail a bien été envoyé
        if(mail($to, $subject, $message_client, $headers)){
            $message_mail = "Votre e-mail a bien été envoyé."; // Stocker dans une variable un message
            http_response_code(200); // Renvoyer une réponse positive au serveur
            echo $message_mail; // Afficher dans la console le message
        } else {
            $message_mail = "Votre e-mail n'a pas été envoyé, remplissez tous les champs.";
            http_response_code(500); // Renvoyer une réponse négative au serveur
            echo $message_mail;
        }


        /* ----- Envoi d'un email de confirmation de la demande au client ----- */
        $to2= $email; // Mail du client
        $subject2="[Serv'Drone] Message de confirmation d'envoi d'une demande"; // Sujet du mail
        $message_confirm = "Votre message a bien été envoyé à Serv'Drone.
        Contenu du mail :
        Type de demande : $type_demande
        Nom de l'utilisateur : $nom
        Prénom de l'utilisateur : $prenom
        Message de l'utilisateur :
        $message"; // Message du mail

        // Informations : le mail va être envoyé à qui, on peut ajouter des cc/cci, quannd on clique sur répondre, répondre à qui + version de php
        $email_response = "servdrone@contact.com";

        $headers2 = 'From: servdrone@contact.com' . "\r\n" .
        'Reply-To: ' . $email_response . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

        // Si le mail a bien été envoyé
        if(mail($to2, $subject2, $message_confirm, $headers2)){
            $message_mail2 = "L'email de confirmation a été envoyé"; // Stocker dans une variable un message
            http_response_code(200); // Renvoyer une réponse positive au serveur
            echo $message_mail2; // Afficher dans la console le message
        } else {
            $message_mail2 = "L'email de confirmation n'a pas été envoyé, remplissez tous les champs.";
            http_response_code(500); // Renvoyer une réponse négative au serveur
            echo $message_mail2;
        }
    }
?>