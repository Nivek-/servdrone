"use strict";

document.addEventListener('DOMContentLoaded', function () {
  // Une fois que la page est prête (a chargé)
  var btn_menu_hamburger = document.getElementById("menu-hamburger");
  var menu_hamburger = document.getElementsByTagName("header")[0];
  var button_close_menu = document.getElementById("button-close-menu"); // Lors du clic sur le bouton "#menu-hamburger", ouvrir le menu

  btn_menu_hamburger.addEventListener("click", function (event) {
    event.stopPropagation();
    menu_hamburger.classList.add("menu-open"); // Ajouter la class menu-open

    btn_menu_hamburger.classList.add("button-hamburger-hidden"); // Ajouter la class button-hamburger-hidden
  }); // Lors du clic sur le bouton "#button-close-menu", fermer le menu

  button_close_menu.addEventListener("click", function () {
    menu_hamburger.classList.remove("menu-open"); // Retirer la class menu-open

    btn_menu_hamburger.classList.remove("button-hamburger-hidden"); // Retirer la class button-hamburger-hidden
  }); // Lors du click sur le body, fermer le menu

  document.getElementsByTagName("body")[0].addEventListener("click", function () {
    if (menu_hamburger.className == "menu-open") {
      menu_hamburger.classList.remove("menu-open");
      btn_menu_hamburger.classList.remove("button-hamburger-hidden"); // Retirer la class button-hamburger-hidden
    }
  }); //Lors du clic sur "#navbar", stopper la propagation

  document.getElementsByTagName("header")[0].addEventListener("click", function (event) {
    event.stopPropagation();
  });
});